#include <LBattery.h>

char buff[256];

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
   pinMode(13, OUTPUT);
   pinMode(1, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  if (LBattery.level() > 50)
  {
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);              // wait for a second
  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);              // wait for a second
    }
    else
    {
      digitalWrite(1, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);              // wait for a second
  digitalWrite(1, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);
      
      
      }
  sprintf(buff,"battery level = %d", LBattery.level() );
  Serial.println(buff);
  sprintf(buff,"is charging = %d",LBattery.isCharging() );
  Serial.println(buff);
 delay(1000); 
}

