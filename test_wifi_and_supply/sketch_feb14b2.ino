
/*
 This program allow you to connect to an AP and gives you some informations about it
 */
#include <LBattery.h>
char buff[256];
#include <LWiFi.h>
#include <LWiFiClient.h>
#define WIFI_AP "Lebon"
#define WIFI_PASSWORD ""
#define WIFI_AUTH LWIFI_OPEN  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP.
void setup()
{
  LWiFi.begin();
  Serial.begin(115200);
  pinMode(13, OUTPUT);
   pinMode(1, OUTPUT);

  // keep retrying until connected to AP
  Serial.println("Connecting to AP");
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
  {
    delay(1000);
  }
  //keep retrying until connected to website
  printWifiStatus();
  
  
}
void loop()
{
  
  long rssi = LWiFi.RSSI();
  Serial.print(rssi);
  Serial.println(" dBm");
  delay(2000);

    // put your main code here, to run repeatedly: 
    if (LBattery.level() > 50)
    {
      digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(1000);              // wait for a second  
      digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW 
      delay(1000);              // wait for a second   
      }   
      else  
      {      
        digitalWrite(1, HIGH);   // turn the LED on (HIGH is the voltage level)
        delay(1000);              // wait for a second  
        digitalWrite(1, LOW);    // turn the LED off by making the voltage LOW 
        delay(1000);                 
        }  
        sprintf(buff,"battery level = %d", LBattery.level() ); 
        Serial.println(buff); 
        sprintf(buff,"is charging = %d",LBattery.isCharging() ); 
        Serial.println(buff);
        delay(1000); 
    }

void printWifiStatus()
{
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());
  // print your WiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  Serial.print("subnet mask: ");
  Serial.println(LWiFi.subnetMask());
  Serial.print("gateway IP: ");
  Serial.println(LWiFi.gatewayIP());
  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
